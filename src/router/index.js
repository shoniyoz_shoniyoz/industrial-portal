import { createRouter, createWebHistory } from "vue-router";

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: "/login",
      name: "login",
      component: () => import("../pages/login.vue"),
      meta: {
        layout: "Default",
      },
    },
    {
      path: "/",
      name: "home",
      component: () => import("../pages/home/index.vue"),
      meta: {
        layout: "Main",
      },
    },
  ],
});

export default router;
